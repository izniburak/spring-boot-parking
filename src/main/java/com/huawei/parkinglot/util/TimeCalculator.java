package com.huawei.parkinglot.util;

import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeCalculator {

    @Getter @Setter
    private String timeFormat = "yyyy/MM/dd HH:mm:ss";

    private final SimpleDateFormat simpleDate;

    @Getter @Setter
    private String to;

    @Getter @Setter
    private String from;

    @Getter
    private long hour;

    public TimeCalculator() {
        simpleDate = new SimpleDateFormat(timeFormat);
    }

    public TimeCalculator to(String time) {
        to = time;
        return this;
    }

    public TimeCalculator from(String time) {
        from = time;
        return this;
    }

    public TimeCalculator calc() {
        try {
            Date d1 = simpleDate.parse(to);
            Date d2 = simpleDate.parse(from);
            long diff = d2.getTime() - d1.getTime();
            hour = diff / (60 * 60 * 1000);
        } catch (Exception e) {
            e.printStackTrace();
            hour = 0;
        }
        return this;
    }
}
