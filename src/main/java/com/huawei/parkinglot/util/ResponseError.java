package com.huawei.parkinglot.util;

import lombok.Data;

import java.util.List;

@Data
public class ResponseError<T> extends Response<T> {

    private List<ErrorDetails> errors;

    public ResponseError(String message) {
        super(message);
        success = false;
    }

    public ResponseError(T data) {
        super(data);
        success = false;
    }

    public ResponseError(T data, String message) {
        super(data, message);
        success = false;
    }

    @Data
    public static class ErrorDetails {
        private String fieldName;
        private String message;
    }

}
