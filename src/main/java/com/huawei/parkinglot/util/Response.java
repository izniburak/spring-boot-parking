package com.huawei.parkinglot.util;

import lombok.Getter;
import lombok.Setter;

@Getter
public class Response<T> {

    protected Boolean success = true;
    protected String message = null;
    protected T data;

    public Response(String message) {
        this.message = message;
    }

    public Response(T data) {
        this.data = data;
    }

    public Response(T data, String message) {
        this.message = message;
        this.data = data;
    }

}
