package com.huawei.parkinglot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
@Table(name = "parking_areas")
@EntityListeners(AuditingEntityListener.class)
public class ParkingArea {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column(nullable = false, length = 100)
    private String name;

    @Column(nullable = false, length = 5)
    private Integer capacity;

    @Column(nullable = false, length = 50)
    private String city;

    @OneToMany(mappedBy = "parkingArea", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<Price> prices;

    @JsonIgnore
    @OneToMany(mappedBy = "parkingArea", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<ParkingCheck> parkingChecks;

}
