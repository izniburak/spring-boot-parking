package com.huawei.parkinglot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Data
@Entity
@Table(name = "prices")
@EntityListeners(AuditingEntityListener.class)
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "parking_area_id")
    @JsonIgnore
    private ParkingArea parkingArea;

    @Column(nullable = false, length = 4)
    private Double price;

    @Column(nullable = false, length = 2, name = "hour_from")
    private Integer from;

    @Column(nullable = false, length = 2, name = "hour_to")
    private Integer to;
}
