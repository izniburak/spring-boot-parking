package com.huawei.parkinglot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.huawei.parkinglot.entity.vehicle.Vehicle;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Data
@Entity
@Table(name = "parking_checks")
@EntityListeners(AuditingEntityListener.class)
public class ParkingCheck {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column(nullable = false)
    private String checkIn;

    @Column
    private String checkOut;

    @Column
    private Double fee;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "parking_area_id")
    @JsonIgnore
    private ParkingArea parkingArea;

}
