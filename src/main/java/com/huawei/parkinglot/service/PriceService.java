package com.huawei.parkinglot.service;

import com.huawei.parkinglot.entity.Price;
import com.huawei.parkinglot.repository.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PriceService implements IPriceService {

    @Autowired
    private PriceRepository priceRepository;

    @Override
    public Price save(Price price) {
        return priceRepository.save(price);
    }

    @Override
    public Boolean deleteAllPriceByParkingAreaId(Long parkingAreaId) throws Exception {
        try {
            priceRepository.deleteByParkingAreaId(parkingAreaId);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
