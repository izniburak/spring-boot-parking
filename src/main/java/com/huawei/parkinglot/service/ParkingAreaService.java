package com.huawei.parkinglot.service;

import com.huawei.parkinglot.controller.request.ParkingAreaRequest;
import com.huawei.parkinglot.entity.ParkingArea;
import com.huawei.parkinglot.entity.ParkingCheck;
import com.huawei.parkinglot.entity.Price;
import com.huawei.parkinglot.entity.vehicle.Vehicle;
import com.huawei.parkinglot.exception.ResourceNotFoundException;
import com.huawei.parkinglot.repository.ParkingAreaRepository;
import com.huawei.parkinglot.util.TimeCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ParkingAreaService implements IParkingAreaService {

    @Autowired
    private ParkingAreaRepository parkingAreaRepository;

    @Autowired
    private IPriceService priceService;

    @Autowired
    private IParkingCheckService parkingCheckService;

    @Autowired
    private IVehicleService vehicleService;

    public ParkingArea findById(Long id) throws Exception {
        Optional<ParkingArea> result = parkingAreaRepository.findById(id);
        if (!result.isPresent()) {
            throw new Exception("Could not find parking area with given id - " + id);
        }
        return result.get();
    }

    @Override
    public List<ParkingArea> findAll() {
        return parkingAreaRepository.findAll();
    }

    @Override
    public ParkingArea save(ParkingArea parkingArea) {
        ParkingArea newParkingArea = new ParkingArea();
        newParkingArea.setName(parkingArea.getName());
        newParkingArea.setCapacity(parkingArea.getCapacity());
        newParkingArea.setCity(parkingArea.getCity());
        newParkingArea = parkingAreaRepository.save(newParkingArea);

        ParkingArea finalNewParkingArea = newParkingArea;
        parkingArea.getPrices().forEach(price -> {
            price.setParkingArea(finalNewParkingArea);
            priceService.save(price);
        });

        return newParkingArea;
    }

    @Override
    public ParkingArea update(Long id, ParkingArea parkingArea) throws Exception {
        ParkingArea existsParkingArea = findById(id);

        existsParkingArea.setName(parkingArea.getName());
        existsParkingArea.setCapacity(parkingArea.getCapacity());
        existsParkingArea.setCity(parkingArea.getCity());

        priceService.deleteAllPriceByParkingAreaId(id);
        parkingArea.getPrices().forEach(price -> {
            price.setParkingArea(existsParkingArea);
            priceService.save(price);
        });

        return parkingAreaRepository.save(existsParkingArea);
    }

    @Override
    public Boolean deleteById(Long id) {
        try {
            parkingAreaRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Boolean checkIn(Long id, Vehicle vehicle, String time) throws Exception {
        ParkingArea parkingArea = findById(id);
        Vehicle parkedVehicle;
        try {
            parkedVehicle = vehicleService.findByLicensePlate(vehicle.getLicensePlate());
        } catch (ResourceNotFoundException e) {
            parkedVehicle = vehicleService.save(vehicle);
        }

        ParkingCheck parking = new ParkingCheck();
        parking.setCheckIn(time);
        parking.setParkingArea(parkingArea);
        parking.setVehicle(parkedVehicle);
        parkingCheckService.save(parking);

        return true;
    }

    @Override
    public Boolean checkOut(Long id, Vehicle vehicle, String time) throws Exception {
        findById(id);
        ParkingCheck parkingCheck = parkingCheckService.getLatestActiveParking(vehicle.getLicensePlate());
        parkingCheck.setCheckOut(time);

        TimeCalculator timeCalculator = new TimeCalculator();
        timeCalculator.from(parkingCheck.getCheckIn()).to(parkingCheck.getCheckOut()).calc();

        Price price = parkingAreaRepository.getCalculatedPrice(id, timeCalculator.getHour());

        parkingCheck.setFee(price.getPrice());
        parkingCheckService.save(parkingCheck);
        return true;
    }

    @Override
    public Collection<Vehicle> getAllActiveVehicles(Long parkingAreaId) {
        return parkingCheckService.getAllActiveVehicles(parkingAreaId);
    }

    @Override
    public Collection<ParkingCheck> getAllParkHistory(Long parkingAreaId) {
        return parkingCheckService.getAllParkHistory(parkingAreaId);
    }

    @Override
    public ParkingArea prepareData(ParkingAreaRequest request, Long id)
    {
        ParkingArea parkingArea = new ParkingArea();
        parkingArea.setName(request.getName());
        parkingArea.setCapacity(request.getCapacity());
        parkingArea.setCity(request.getCity());

        Collection<Price> prices = new ArrayList<>();
        request.getPrices().forEach(price -> {
            Price newPrice = new Price();
            newPrice.setPrice(Double.parseDouble(price.get("price").toString()));
            newPrice.setFrom(Integer.parseInt(price.get("from").toString()));
            newPrice.setTo(Integer.parseInt(price.get("to").toString()));
            prices.add(newPrice);
        });
        parkingArea.setPrices(prices);

        return parkingArea;
    }
}
