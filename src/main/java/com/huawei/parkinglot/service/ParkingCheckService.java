package com.huawei.parkinglot.service;

import com.huawei.parkinglot.entity.ParkingCheck;
import com.huawei.parkinglot.entity.vehicle.Vehicle;
import com.huawei.parkinglot.repository.ParkingCheckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class ParkingCheckService implements IParkingCheckService {

    @Autowired
    private ParkingCheckRepository parkingCheckRepository;

    @Autowired
    private IVehicleService vehicleService;

    @Override
    public ParkingCheck save(ParkingCheck parkingCheck) {
        return parkingCheckRepository.save(parkingCheck);
    }

    @Override
    public ParkingCheck getLatestActiveParking(String licensePlate) {
        Vehicle vehicle = vehicleService.findByLicensePlate(licensePlate);
        return vehicleService.getLatestParking(vehicle.getLicensePlate());
    }

    @Override
    public Collection<ParkingCheck> getAllParking(String licensePlate) {
        Vehicle vehicle = vehicleService.findByLicensePlate(licensePlate);
        return vehicle.getParkingChecks();
    }

    @Override
    public Collection<Vehicle> getAllActiveVehicles(Long parkingAreaId) {
        Collection<ParkingCheck> items = parkingCheckRepository.findByParkingAreaId(parkingAreaId);
        Collection<Vehicle> vehicles = new ArrayList<>();
        items.forEach(item -> {
            vehicles.add(item.getVehicle());
        });
        return vehicles;
    }

    @Override
    public Collection<ParkingCheck> getAllParkHistory(Long parkingAreaId) {
        return parkingCheckRepository.findByParkingAreaId(parkingAreaId);
    }
}
