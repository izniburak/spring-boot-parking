package com.huawei.parkinglot.service;

import com.huawei.parkinglot.entity.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {
    Optional<User> getByEmail(String email);
    User findById(Long id) throws Exception;
    List<User> findAll();
    User save(User user);
    User update(Long id, User user) throws Exception;
    Boolean deleteById(Long id) throws Exception;
}
