package com.huawei.parkinglot.service;

import com.huawei.parkinglot.entity.Price;

public interface IPriceService {

    Price save(Price price);

    Boolean deleteAllPriceByParkingAreaId(Long parkingAreaId) throws Exception;

}
