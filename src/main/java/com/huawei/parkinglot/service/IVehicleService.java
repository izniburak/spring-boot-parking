package com.huawei.parkinglot.service;

import com.huawei.parkinglot.entity.ParkingCheck;
import com.huawei.parkinglot.entity.vehicle.Vehicle;
import com.huawei.parkinglot.exception.ResourceNotFoundException;

import java.util.List;

public interface IVehicleService {

    List<Vehicle> findAll();

    Vehicle findByLicensePlate(String licensePlate) throws ResourceNotFoundException;

    Vehicle save(Vehicle vehicle);

    ParkingCheck getLatestParking(String licensePlate);

}
