package com.huawei.parkinglot.service;

import com.huawei.parkinglot.entity.User;
import com.huawei.parkinglot.exception.ResourceNotFoundException;
import com.huawei.parkinglot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Optional<User> getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findById(Long id) throws Exception {
        Optional<User> result = userRepository.findById(id);
        if (!result.isPresent()) {
            throw new ResourceNotFoundException("User", "ID", id);
        }
        return result.get();
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User save(User user) {
        if (!user.getPassword().isEmpty()) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        return userRepository.save(user);
    }

    @Override
    public User update(Long id, User user) throws Exception {
        User currentUser = findById(id);
        currentUser.setEmail(user.getEmail());
        if (!user.getPassword().isEmpty()) {
            currentUser.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        return userRepository.save(currentUser);
    }

    @Override
    public Boolean deleteById(Long id) throws Exception {
        User user = findById(id);
        userRepository.deleteById(user.getId());
        return true;
    }

}
