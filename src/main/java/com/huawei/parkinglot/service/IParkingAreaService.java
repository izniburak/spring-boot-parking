package com.huawei.parkinglot.service;

import com.huawei.parkinglot.controller.request.ParkingAreaRequest;
import com.huawei.parkinglot.entity.ParkingArea;
import com.huawei.parkinglot.entity.ParkingCheck;
import com.huawei.parkinglot.entity.vehicle.Vehicle;

import java.util.Collection;
import java.util.List;

public interface IParkingAreaService {
    ParkingArea findById(Long id) throws Exception;

    List<ParkingArea> findAll();

    ParkingArea save(ParkingArea parkingArea);

    ParkingArea update(Long id, ParkingArea parkingArea) throws Exception;

    Boolean deleteById(Long id);

    Boolean checkIn(Long id, Vehicle vehicle, String time) throws Exception;

    Boolean checkOut(Long id, Vehicle vehicle, String time) throws Exception;

    Collection<Vehicle> getAllActiveVehicles(Long parkingAreaId) throws Exception;

    Collection<ParkingCheck> getAllParkHistory(Long parkingAreaId) throws Exception;

    ParkingArea prepareData(ParkingAreaRequest request, Long id);
}
