package com.huawei.parkinglot.service;

import com.huawei.parkinglot.entity.ParkingCheck;
import com.huawei.parkinglot.entity.vehicle.Vehicle;
import com.huawei.parkinglot.exception.ResourceNotFoundException;
import com.huawei.parkinglot.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleService implements IVehicleService {

	@Autowired
	private VehicleRepository vehicleRepository;

	@Override
	public List<Vehicle> findAll() {
		return vehicleRepository.findAll();
	}

	@Override
	public Vehicle findByLicensePlate(String licensePlate) throws ResourceNotFoundException {
		Optional<Vehicle> result = vehicleRepository.findByLicensePlate(licensePlate);
		if (!result.isPresent()) {
			throw new ResourceNotFoundException("Vehicle", "licensePlate", licensePlate);
		}
		return result.get();
	}

	@Override
	public Vehicle save(Vehicle vehicle) {
		return vehicleRepository.save(vehicle);
	}

	@Override
	public ParkingCheck getLatestParking(String licensePlate) {
		return vehicleRepository.getLatestParkCheck(licensePlate);
	}
}
