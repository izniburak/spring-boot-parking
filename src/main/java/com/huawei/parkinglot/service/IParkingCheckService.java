package com.huawei.parkinglot.service;

import com.huawei.parkinglot.entity.ParkingCheck;
import com.huawei.parkinglot.entity.vehicle.Vehicle;

import java.util.Collection;

public interface IParkingCheckService {

    ParkingCheck save(ParkingCheck parkingCheck);

    ParkingCheck getLatestActiveParking(String licensePlate);

    Collection<ParkingCheck> getAllParking(String licensePlate);

    Collection<Vehicle> getAllActiveVehicles(Long parkingAreaId);

    Collection<ParkingCheck> getAllParkHistory(Long parkingAreaId);

}
