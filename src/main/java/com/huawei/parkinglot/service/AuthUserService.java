package com.huawei.parkinglot.service;

import com.huawei.parkinglot.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class AuthUserService implements UserDetailsService {

    @Autowired
    private IUserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> user =  userService.getByEmail(email);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException("User not found with email: " + email);
        }

        User userDetail = user.get();
        return new org.springframework.security.core.userdetails.User(
                userDetail.getEmail(), userDetail.getPassword(), new ArrayList<>()
        );
    }

    public User register(User user) {
        return userService.save(user);
    }
}
