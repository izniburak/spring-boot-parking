package com.huawei.parkinglot.repository;

import com.huawei.parkinglot.entity.ParkingArea;
import com.huawei.parkinglot.entity.ParkingCheck;
import com.huawei.parkinglot.entity.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingAreaRepository extends JpaRepository<ParkingArea, Long> {

    @Query(nativeQuery=true, value = "SELECT p.price FROM prices p WHERE p.parking_area_id = :parkingArea AND p.hour_from >= :hour ORDER BY p.hour_from ASC LIMIT 1")
    Price getCalculatedPrice(@Param("parkingArea") Long parkingAreaId, @Param("hour") long hour);

}
