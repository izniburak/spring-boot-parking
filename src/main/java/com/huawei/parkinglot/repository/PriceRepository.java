package com.huawei.parkinglot.repository;

import com.huawei.parkinglot.entity.Price;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PriceRepository extends JpaRepository<Price, Long> {
    void deleteByParkingAreaId(Long parkingAreaId);
}
