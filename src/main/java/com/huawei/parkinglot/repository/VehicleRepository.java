package com.huawei.parkinglot.repository;

import com.huawei.parkinglot.entity.ParkingCheck;
import com.huawei.parkinglot.entity.vehicle.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    Optional<Vehicle> findByLicensePlate(String licensePlate);

    @Query(nativeQuery=true, value = "SELECT * FROM parking_checks pc WHERE pc.vehicle_id = :licensePlate AND pc.check_out IS NULL ORDER BY pc.id DESC LIMIT 1")
    ParkingCheck getLatestParkCheck(@Param("licensePlate") String name);
}
