package com.huawei.parkinglot.repository;

import com.huawei.parkinglot.entity.ParkingCheck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ParkingCheckRepository extends JpaRepository<ParkingCheck, Long> {
    Collection<ParkingCheck> findByParkingAreaId(Long parkingAreaId);
}
