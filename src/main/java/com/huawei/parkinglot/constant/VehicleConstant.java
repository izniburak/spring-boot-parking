package com.huawei.parkinglot.constant;

public class VehicleConstant {
    final public static String SUV = "suv";
    final public static String SEDAN = "sedan";
    final public static String MINIVAN = "minivan";
}
