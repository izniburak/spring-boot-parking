package com.huawei.parkinglot.controller;

import com.huawei.parkinglot.entity.User;
import com.huawei.parkinglot.service.IUserService;
import com.huawei.parkinglot.util.Response;
import com.huawei.parkinglot.util.ResponseError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok().body(
                new Response<>(userService.findAll())
        );
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok().body(
                    new Response<>(userService.findById(id))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseError<>(e.getMessage())
            );
        }
    }

    @PostMapping(value = "/")
    @ResponseBody
    public ResponseEntity<?> create(@Valid @RequestBody User user) {
        return ResponseEntity.ok().body(
                new Response<>(userService.save(user))
        );
    }

    @PutMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<?> update(@PathVariable("id") Long id, @Valid @RequestBody User user) {
        try {
            return ResponseEntity.ok().body(
                new Response<>(userService.update(id, user))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }

    @DeleteMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok().body(
                new Response<>(userService.deleteById(id))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }

}
