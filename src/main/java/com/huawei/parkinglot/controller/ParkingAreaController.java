package com.huawei.parkinglot.controller;

import com.huawei.parkinglot.controller.request.CheckInRequest;
import com.huawei.parkinglot.controller.request.CheckOutRequest;
import com.huawei.parkinglot.controller.request.ParkingAreaRequest;
import com.huawei.parkinglot.entity.ParkingArea;
import com.huawei.parkinglot.entity.vehicle.Vehicle;
import com.huawei.parkinglot.service.IParkingAreaService;
import com.huawei.parkinglot.util.Response;
import com.huawei.parkinglot.util.ResponseError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/parking-areas")
public class ParkingAreaController {

    @Autowired
    private IParkingAreaService parkingAreaService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok().body(
            new Response<>(parkingAreaService.findAll())
        );
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok().body(
                new Response<>(parkingAreaService.findById(id))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }

    @PostMapping(value = "/")
    @ResponseBody
    public ResponseEntity<?> create(@Valid @RequestBody ParkingAreaRequest request) {
        ParkingArea parkingArea = parkingAreaService.prepareData(request, null);
        return ResponseEntity.ok().body(
            new Response<>(parkingAreaService.save(parkingArea))
        );
    }

    @PutMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<?> update(@PathVariable("id") Long id, @Valid @RequestBody ParkingAreaRequest request) throws Exception {
        ParkingArea parkingArea = parkingAreaService.prepareData(request, id);
        return ResponseEntity.ok().body(
            new Response<>(parkingAreaService.update(id, parkingArea))
        );
    }

    @DeleteMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        return ResponseEntity.ok().body(
            new Response<>(parkingAreaService.deleteById(id))
        );
    }

    @GetMapping(value = "/{id}/vehicles")
    @ResponseBody
    public ResponseEntity<?> getVehicles(@PathVariable("id") Long parkingAreaId) {
        try {
            return ResponseEntity.ok().body(
                new Response<>(parkingAreaService.getAllActiveVehicles(parkingAreaId))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }

    @GetMapping(value = "/{id}/history")
    @ResponseBody
    public ResponseEntity<?> getHistory(@PathVariable("id") Long parkingAreaId) {
        try {
            return ResponseEntity.ok().body(
                new Response<>(parkingAreaService.getAllParkHistory(parkingAreaId))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }

    @PostMapping(value = "/{id}/check-in")
    @ResponseBody
    public ResponseEntity<?> checkIn(@PathVariable("id") Long id, @Valid @RequestBody CheckInRequest request) {
        try {
            Vehicle vehicle = new Vehicle();
            vehicle.setLicensePlate(request.getVehicle().getLicensePlate());
            vehicle.setType(request.getVehicle().getType());
            return ResponseEntity.ok().body(
                new Response<>(parkingAreaService.checkIn(id, vehicle, request.getTime()))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }

    @PostMapping(value = "/{id}/check-out")
    @ResponseBody
    public ResponseEntity<?> checkOut(@PathVariable("id") Long id, @Valid @RequestBody CheckOutRequest request) {
        try {
            Vehicle vehicle = new Vehicle();
            vehicle.setLicensePlate(request.getLicensePlate());
            return ResponseEntity.ok().body(
                new Response<>(parkingAreaService.checkOut(id, vehicle, request.getTime()))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }

}
