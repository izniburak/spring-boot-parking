package com.huawei.parkinglot.controller;

import com.huawei.parkinglot.controller.request.AuthRequest;
import com.huawei.parkinglot.controller.request.UserRequest;
import com.huawei.parkinglot.entity.User;
import com.huawei.parkinglot.service.AuthUserService;
import com.huawei.parkinglot.util.JwtUtil;
import com.huawei.parkinglot.util.Response;
import com.huawei.parkinglot.util.ResponseError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AuthUserService authUserService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthRequest request) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword())
            );

            final UserDetails userDetails = authUserService.loadUserByUsername(request.getEmail());
            final String jwt = jwtUtil.generateToken(userDetails);

            HashMap<String, String> response = new HashMap<>();
            response.put("access_token", jwt);

            return ResponseEntity.ok().body(new Response<>(response, "Successfully logged in."));
        }
        catch (BadCredentialsException | UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody AuthRequest request) {
        try {
            User user = new User();
            user.setEmail(request.getEmail());
            user.setPassword(request.getPassword());
            return ResponseEntity.ok().body(
                new Response<>(authUserService.register(user), "New user added.")
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }
}

