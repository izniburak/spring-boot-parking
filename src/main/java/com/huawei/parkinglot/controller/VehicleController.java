package com.huawei.parkinglot.controller;

import com.huawei.parkinglot.service.IParkingCheckService;
import com.huawei.parkinglot.service.IVehicleService;
import com.huawei.parkinglot.util.Response;
import com.huawei.parkinglot.util.ResponseError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    @Autowired
    private IVehicleService vehicleService;

    @Autowired
    private IParkingCheckService parkingCheckService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok().body(
            new Response<>(vehicleService.findAll())
        );
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<?> get(@PathVariable("id") String licensePlate) {
        try {
            return ResponseEntity.ok().body(
                new Response<>(vehicleService.findByLicensePlate(licensePlate))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }

    @GetMapping(value = "/{id}/parks")
    @ResponseBody
    public ResponseEntity<?> getParkList(@PathVariable("id") String licensePlate) {
        try {
            return ResponseEntity.ok().body(
                new Response<>(parkingCheckService.getAllParking(licensePlate))
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseError<>(e.getMessage())
            );
        }
    }
}
