package com.huawei.parkinglot.controller.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class CheckInRequest {

    @NotNull(message = "Time is mandatory")
    @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}\\s+\\d{2}:\\d{2}", message = "Time must be YYYY-MM-DD HH:SS format")
    private String time;

    @NotNull(message = "Vehicle is mandatory")
    private VehicleRequest vehicle;

}
