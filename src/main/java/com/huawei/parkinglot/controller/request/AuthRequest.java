package com.huawei.parkinglot.controller.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class AuthRequest {

    @NotBlank(message = "Email is mandatory")
    @Email(message = "Email must be valid format")
    private String email;

    @NotBlank(message = "Password is mandatory")
    @Size(min = 6, max = 24, message = "Password must be between 6 and 24 chars")
    private String password;

}
