package com.huawei.parkinglot.controller.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class CheckOutRequest {

    @NotBlank(message = "Time is mandatory")
    @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}\\s+\\d{2}:\\d{2}", message = "Time must be YYYY-MM-DD HH:SS format")
    private String time;

    @NotBlank(message = "License Plate is mandatory")
    @Size(min = 5, max = 10)
    private String licensePlate;

}
