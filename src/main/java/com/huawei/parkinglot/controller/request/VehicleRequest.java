package com.huawei.parkinglot.controller.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class VehicleRequest {
    @NotBlank
    @Size(min = 5, max = 10)
    String licensePlate;

    @NotEmpty(message = "Vehicle Type is mandatory")
    String type;
}
