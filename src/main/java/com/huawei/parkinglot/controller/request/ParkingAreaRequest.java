package com.huawei.parkinglot.controller.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;

@Data
public class ParkingAreaRequest {
    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotNull
    @Min(value = 50, message = "Capacity should be minimum 50.")
    private Integer capacity;

    @NotBlank(message = "City is mandatory")
    private String city;

    @NotEmpty(message = "Price List cannot be empty.")
    private ArrayList<HashMap<String, Object>> prices;
}
