package com.huawei.parkinglot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class ServerErrorException extends RuntimeException {
    private final String message;

    public ServerErrorException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
