## Example Project

### Routes

For Auth:
- Register: POST /auth/register
- Login: POST /auth/login

For Parking Areas:
- GET /parking-areas
- GET /parking-areas/{id}
- POST /parking-areas
- PUT /parking-areas/{id}
- DELETE /parking-areas/{id}
- POST /parking-areas/{id}/check-in - Check-in vehicle
- POST /parking-areas/{id}/check-out - Check-out vehicle
- GET /parking-areas/{id}/vehicles - For active vehicles
- GET /parking-areas/{id}/history - For all history

For Vehicles:
- GET /vehicles
- GET /vehicles/{id}
- GET /vehicles/{id}/parks - All park history for the Vehicle

For Users:
- GET /users
- GET /users/{id}
- POST /users
- PUT /users/{id}
- DELETE /users/{id}
